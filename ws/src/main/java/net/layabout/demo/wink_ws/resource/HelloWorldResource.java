package net.layabout.demo.wink_ws.resource;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/helloworld")
public class HelloWorldResource {

    @Produces(MediaType.TEXT_PLAIN)
    @GET
    public String getMessage() { 
        return "Hello World!";
    }
}
