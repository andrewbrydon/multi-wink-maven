package net.layabout.demo.wink_ws.resource;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import net.layabout.wink_ws.data.todo.Todo;

@Path("/todo")
public class TodoResource {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Todo getNew() {
        return new Todo();
    }
}
