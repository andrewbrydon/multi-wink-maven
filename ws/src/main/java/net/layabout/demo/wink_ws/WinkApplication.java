package net.layabout.demo.wink_ws;

import java.util.Set;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

import net.layabout.demo.wink_ws.resource.HelloWorldResource;
import net.layabout.demo.wink_ws.resource.TodoResource;

import com.fasterxml.jackson.jaxrs.json.JacksonJaxbJsonProvider;
import com.google.common.collect.ImmutableSet;

@ApplicationPath("/*")
public class WinkApplication extends Application {

    @Override
    public Set<Class<?>> getClasses() {
        return ImmutableSet.<Class<?>>of(
                HelloWorldResource.class,
                TodoResource.class);
    }
    
    @Override
    public Set<Object> getSingletons() {
      return ImmutableSet.<Object>of(new JacksonJaxbJsonProvider());
    }
}
