package net.layabout.demo.wink_ws;

import net.layabout.demo.wink_ws.common.guice.CommonUtilsGuiceModule;

import org.apache.wink.guice.server.internal.GuiceDeploymentConfiguration;
import org.apache.wink.guice.server.internal.lifecycle.WinkGuiceModule;

import com.google.inject.Module;

public class DeploymentConfiguration extends GuiceDeploymentConfiguration {

    @Override
    public Module[] createModules() {
        return new Module[] { new WinkGuiceModule(), new CommonUtilsGuiceModule() };
    }
}
