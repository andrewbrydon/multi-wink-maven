package net.layabout.demo.wink_ws.common.utils;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class CurrentTimeProviderTest {

    @Test
    public void testGetReturnsCurrentTimeMillis() {
        CurrentTimeProvider testObject = new CurrentTimeProvider();
        
        long beforeTimeMillis = System.currentTimeMillis();
        long result = testObject.get();
        long afterTimeMillis = System.currentTimeMillis();
        
        assertTrue("expect result bounded by current time in millis",
                beforeTimeMillis <= result && result <= afterTimeMillis);
    }

}
