package net.layabout.demo.wink_ws.common.guice;

import net.layabout.demo.wink_ws.common.utils.CurrentTimeProvider;

import com.google.inject.AbstractModule;
import com.google.inject.name.Names;

public class CommonUtilsGuiceModule extends AbstractModule {

    @Override
    protected void configure() {
        bind(Long.class).annotatedWith(Names.named("current.time.millis")).toProvider(CurrentTimeProvider.class);
    }

}
