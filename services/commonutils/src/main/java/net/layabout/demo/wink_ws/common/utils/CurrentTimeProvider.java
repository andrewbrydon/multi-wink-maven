package net.layabout.demo.wink_ws.common.utils;

import javax.inject.Provider;
import javax.inject.Singleton;

@Singleton
public class CurrentTimeProvider implements Provider<Long> {

    @Override
    public Long get() {
        return System.currentTimeMillis();
    }

}
